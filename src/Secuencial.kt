package foo


class Secuencias
{
    //Programación en estructura secuencial.

    /*
    1. Programa Kotlin que lea dos números enteros
       por teclado y los muestre por pantalla.
     */
    fun E1()
    {
        //Read two values in a same line and convert them at same time
        val (A,B) = readLine()!!.split(' ').map(String::toInt)

        println(A+B)
    }

    /*
    2. Programa Kotlin que lea un nombre y muestre por pantalla:
       “Buenos dias nombre_introducido”.
     */
    fun E2()
    {
        val name = readLine()!!.toString()

        println("Good morning $name!")
    }

    /*
    3. Programa Kotlin que lee un número entero por teclado y
       obtiene y muestra por pantalla el doble y el triple de ese número.
     */
    fun E3()
    {
        val number = readLine()!!.toInt()
        var n1 = number * 2
        var n2 = number * 3

        println("The double number is: $n1\nThe triple number is: $n2")

    }

    /*
    4. Programa que lea una cantidad de grados centígrados y la pase
       a grados Fahrenheit. La fórmula correspondiente es: F = 32 + ( 9 * C / 5)
     */
    fun E4()
    {
        println("Type a valid centigrade value")
        val cent = readLine()!!.toInt()
        val faren = 32+(9*cent/5)

        println("Celcious to fahrenheit is: $faren")
    }

    /*
    5. Programa que lee por teclado el valor del radio de una circunferencia
       y calcula y muestra por pantalla la longitud y el área de la
       circunferencia. Longitud de la circunferencia = 2*PI*Radio,
       Area de la circunferencia = PI*Radio^2
    * */
    fun E5()
    {
        println("Type ratio of circunference")

        val R = readLine()!!.toDouble()

        val Long = 2*3.1416*R
        val Area = 3.1416*R*R

        println("The width is: $Long\nThe ares is: $Area")

    }

    /*
    6. Programa que pase una velocidad en Km/h a m/s.
       La velocidad se lee por teclado.
    * */
    fun E6()
    {
        println("Type speed in Km/h")

        val km = readLine()!!.toDouble()
        val mt = km*1000

        println("Km/h to m/s: $mt m/s")
    }

    /*
    7. Programa lea la longitud de los catetos de un triángulo rectángulo y
       calcule la longitud de la hipotenusa según el teorema de Pitágoras.
     */
    fun E7()
    {
        println("Ingrese cateto A y B")

        val (A,B) = readLine()!!.split(' ').map(String::toDouble)

        val Hipo = Math.sqrt((A*A)+(B*B))

        println("La hipotenusa es: $Hipo")
    }

    /*
    10. Programa que lee un número de 3 cifras y
        muestra sus cifras por separado.
     */
    fun E10()
    {
        println("Type 3 digits number")

        val Number = readLine()!!.toInt()
        val n1 = Number/100
        val n2 = (Number/10)%10
        val n3 = Number%10

        println("First digit: $n1\nSecond digit: $n2\nThird digit: $n3")
    }

}