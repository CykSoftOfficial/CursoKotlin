import EPOO.Contador
import EPOO.Cuenta
import EPOO.Libro

fun main(args: Array<String>){

    println("Hola gurrupletas\n")

}

fun E1()
{
    var Cliente1 = Cuenta("Mateo", "10010", 10000.0)
    var Cliente2 = Cuenta("Sara", "20010", 6000.0)

    var Cliente3 = Cuenta()
    Cliente3.Nombre = "David"
    Cliente3.NumeroCuenta = "30010"
    Cliente3.Salario = 3500.0

    var Cliente4 = Cuenta(Cliente2)

    //Hacemos algunas modificaciones
    Cliente3.Reintegro(1000.0)
    Cliente1.Transferencia(Cliente2, 2000.0)

    var cuentas = arrayListOf<Cuenta>()
    cuentas.add(Cliente1)
    cuentas.add(Cliente2)
    cuentas.add(Cliente3)
    cuentas.add(Cliente4)

    for(x in cuentas)
        println(x.toString())
}

fun E2()
{
    var Libro1 = Libro("Buñuelos", "Diciembre", 2, 0)
    var Libro2 = Libro("Empanadas chidas", "Alberto R", 20, 10)
    var Libro3 = Libro("Como hacer sancocho", "Juan D", 12, 8)
    var Libro4 = Libro("Llama en llamas", "Lamma G", 8, 2)

    var libros = arrayListOf<Libro>()
    libros.add(Libro1)
    libros.add(Libro2)
    libros.add(Libro3)
    libros.add(Libro4)

    for (item in libros)
        println(item.toString())

    Libro1.Prestamo()

    println(Libro1.toString())
}