import com.sun.xml.internal.fastinfoset.util.StringArray
import java.lang.reflect.AnnotatedArrayType

class Arreglos
{
    //Programacion estructurada en arreglos

    /*
    1. Calcular la media de una serie de números almacenados.
    */
    fun E1()
    {
        var sum = 0;
        var prom = 0;

        var nums : IntArray = intArrayOf(45,10,54,12,85,23)

        for(i in 0..nums.size-1)
        {
            sum += nums[i]
        }

        prom = sum / nums.size

        println("The result of average is $prom")


    }

    /*
    2. Calcular la media de una serie de números que se leen por teclado.
    */
    fun E2()
    {
        var sum = 0
        var nums = IntArray(6)

        for(x in nums)
        {
            println("Input an integer number")
            nums.set(x, readLine()!!.toInt())
            //nums[x] = readLine()!!.toInt()
            sum += nums[x]
        }

        val prom = sum / nums.size

        println("The media is: $prom")

    }

    /*
    3. Leer 10 números enteros por teclado y guardarlos en un array. Calcula y muestra
    la media de los números que estén en las posiciones pares del array.
     */

    fun E3()
    {
        var nums = IntArray(10)
        for (i in nums) {
            println("Type a number")
            nums[i] = readLine()!!.toInt()
        }

        var sum = 0

        for(i in nums) {
            if (i % 2 == 0)
                sum += nums[i]
        }

        val med = sum / nums.size

        println("The average is: $med")

    }

    /*
    4. Leer por teclado la nota de los alumnos de una clase y
    calcular la nota media del grupo. Mostar los alumnos con
    notas superiores a la media.
     */

    fun E4()
    {
        var nums = DoubleArray(6)

        for(i in nums)
        {
            println("Ingrese la nota")
            nums[i.toInt()] = readLine()!!.toDouble()
        }
        var sum = 0.0

        for(i in nums)
            sum += nums[i.toInt()]

        val med = sum / nums.size

        for(i in nums)
        {
            if(nums[i.toInt()] > med)
                println("Nota #$i: " + nums[i.toInt()])
        }
    }

    /*
     5. Contar el número de elementos positivos, negativos y ceros en un array de 10 enteros.
     */
    fun E5(){

        var nums = IntArray(10)
        var cp : Int = 0
        var cn : Int = 0
        var co : Int = 0

        for (i in nums)
            nums.set(i,readLine()!!.toInt())
        for(i in nums)
        {
            if(nums.get(i) < 0)
                cn++
            else if(nums.get(i) > 0)
                cp++
            else
                co++
        }

        println("There are $cp positives values")
        println("There are $cn negatives values")
        println("There are $co ZERO values")
    }

    fun E6()
    {
        println("rueba")
    }


}