import com.sun.xml.internal.ws.server.provider.ProviderInvokerTube

class Condicionales
{
    //Programación en estructura condicional


    /*
    1. Programa Kotlin que lea un número entero y calcule si es par o impar.
    * */
    fun E1()
    {
        println("Type an integer number")
        val number = readLine()!!.toInt()

        if(number%2 == 0) println("El número $number es par")
        else println("El número $number es impar")
    }

    /*
    2. Programa que lea un número entero y muestre si el
    número es múltiplo de 10.
    * */
    fun E2()
    {
        println("Type an integer number")
        val Number = readLine()!!.toInt()

        if(Number%10 == 0) println("El número $Number es multiplo de 10")
        else println("El número $Number no es multiplo de 10")
    }

    /*
    4. Programa que lea dos caracteres por teclado y compruebe si son iguales.
    * */
    fun E4()
    {
        println("Type a caracter")
        val(A,B) = readLine()!!.split(' ')

        if(A.equals(B)) println("The caracters are equals")
        else println("The caracters are not equals")
    }

    /*
    7. Programa que lea dos números por teclado y muestre el resultado
    de la división del primero por el segundo. Se debe comprobar que el divisor no puede ser cero.
     */
    fun E7()
    {
        println("Type two numbers")
        val(A,B) = readLine()!!.split(' ').map(String::toInt)

        if(B != 0)
        {
            val result = A/B
            println("The result is: $result")
        }
        else println("Divide by zero exception :v")
    }

    /*
    8. Calcular el mayor de tres números enteros en Kotlin.
     */
    fun E8()
    {
        println("Ingresar tres enteros")
        val(A,B,C) = readLine()!!.split(' ').map(String::toInt)

        if(A>B && A>C) println("El número $A es el mayor de los tres")
        else if(B>A && B>C) println("El número $B es el mayor de los tres")
        else if(C>A && C>B) println("El número $C es el mayor de los tres")
    }
}