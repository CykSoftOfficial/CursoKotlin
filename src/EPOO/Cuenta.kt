package EPOO

import javafx.scene.Parent

class Cuenta()
{

    var Nombre : String = ""
    var NumeroCuenta : String = ""
    var Salario : Double = 0.0

    constructor(nombre : String, numeroCuenta : String, salario : Double) : this()
    {
        Nombre = nombre
        NumeroCuenta = numeroCuenta
        Salario = salario
    }

    constructor(cuentaCopia : Cuenta) : this()
    {
        Nombre = cuentaCopia.Nombre
        NumeroCuenta = cuentaCopia.NumeroCuenta
        Salario = cuentaCopia.Salario
    }

    fun Ingreso(Monto: Double) = if(Monto >= 0) Salario += Monto else  Salario += 0

    fun Reintegro(Monto: Double) = if(Monto >= 0) Salario += Monto else  Salario -= Monto

    fun Transferencia(cuentaDestino : Cuenta, Importe : Double){ cuentaDestino.Salario += Importe;  Salario -= Importe }

    override fun toString(): String {
        return "Cliente: $Nombre\nNumero de cuenta:. . . .:$NumeroCuenta\nSalario cliente:. . . . .:$$Salario\n::::::::::::::::::::::::::::::::"
    }
}

