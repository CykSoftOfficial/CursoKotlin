package EPOO

class Libro() {

    var Titulo: String = ""
    var Autor: String = ""
    var NPrestados: Int = 0
    var NEjemplares : Int = 0

    constructor(titulo:String, autor:String, nejemplares:Int, nprestados:Int ) : this()
    {
        Titulo = titulo
        Autor = autor
        NPrestados = nprestados
        NEjemplares = nejemplares
    }

    fun Prestamo() : Boolean
    {
        var result = false
        if(NPrestados < NEjemplares)
        {
            NPrestados++
            NEjemplares--
            result = true
        }
        return result
    }

    fun Devolucion() : Boolean
    {
        var result = false
        if(NPrestados > 0)
        {
            NPrestados--
            NEjemplares++
            result = true
        }
        return result
    }

    override fun toString(): String {
        return "Titulo: $Titulo\nAutor: $Autor\nEn prestamos:  $NPrestados\nDisponibles:    $NEjemplares\n:::::::::::::::::::::::::::\n"
    }

}