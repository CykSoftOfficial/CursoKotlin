package EPOO

class Contador()
{
    var Cont : Int = 0

    constructor(c : Int) : this()
    {
        if(c < 0) Cont = 0 else Cont = c
    }

    constructor(copia : Contador) : this()
    {
        Cont = copia.Cont
    }

    fun Incrementar(){ Cont += 1}

    fun Decrementar()
    {
        Cont--
        if(Cont < 0) Cont = 0
    }
}