import java.awt.PageAttributes

class Ciclos
{
    //Programacion iterativa o reetitivas ciclos.

    /*
    1. Programa Kotlin que muestre los números
    del 1 al 100 utilizando la instrucción while
     */
    fun E1()
    {
        var x : Int = 1

        while(x<=100)
        {
            println("Number: $x")
            x++
        }
    }

    /*
    2. Programa Kotlin que muestre los números
       del 1 al 100 utilizando la instrucción do..while
     */
    fun E2()
    {
        var x = 1

        do{
            println("Number: $x")
            x++
        }while(x <= 100)
    }

    /*
    3. Programa Kotlin que muestre los números
       del 1 al 100 utilizando la instrucción for
     */
    fun E3()
    {
        for(i in 1..100) println("Number: $i")
    }

    /*
    4. Programa Kotlin que muestre los números del 100 al 1
       utilizando la instrucción while
     */
    fun E4()
    {
        var x : Int = 100

        while(x >= 1)
        {
            println("Number $x")
            x--
        }
    }

    /*
    5. Programa Kotlin que muestre los
    números del 100 al 1 utilizando la instrucción do..while
    */
    fun E5()
    {
        var x = 100

        do {
            println("Number: $x")
            x--
        }while(x >= 1)
    }

    /*
    6. Programa Kotlin que muestre los números
       del 100 al 1 utilizando la instrucción for
     */
    fun E6()
    {
        for(i in 100 downTo 1) println("Number: $i")
    }

    fun PromedioUniversidad(Materias : Int, TCreditos : Int) : Double{

        var sum = 0.0
        var x = 0

        do {
            println("Ingresar nota y creditos")
            var(A, B) = readLine()!!.split(' ').map(String::toDouble)
            sum += A * B
            x++
        }while(x < 7)

        val result = Math.round(sum / TCreditos).toDouble()

        return result

    }

    fun ReconocerPolindromos(Palabra : String) : Boolean
    {
        var result = false

        var P = ""

        for(x in Palabra.length - 1 downTo 0)
            P += Palabra[x]

        if (Palabra == P)
            result = true

        return result
    }

}